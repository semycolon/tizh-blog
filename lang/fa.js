module.exports = {
    visit_website: "مشاهده وبسایت",
    dashboard: {
        all_posts: 'همه پست‌ها',
        drafts: 'پیش‌نویس‌ها',
        headline: 'نوشته‌های شما',
        no_post: 'پستی وجود ندارد!',
        last_change:'آخرین ویرایش',
        delete: 'حذف پست',
        edit:'ویرایش پست',
        copy_link:"کپی لینک کوتاه",
        published:'مطالب منتشر شده',
        published_at:'منتشر‌شده در',

    },
    publish: 'انتشار عمومی',
    no_title: 'بدون عنوان',
    editor: {
        placeholder: {
            text:'هرچیزی دوست داری بنویس...'
        }
    },
    submit_post: "ثبت مطلب",
    published:'انتشار عمومی',
    slug:'شناسه',
    title: 'عنوان',
    loading: 'درحال بارگزاری',
    edit: "ویرایش",
    logo:'لوگو',
    hello: 'سلام',
    submit:'ثبت',
    reset:'پیشفرض',
    _dashboard:'داشبورد',
    write_new_post:'نوشتن پست جدید',
    _settings:'تنظیمات',
    settings: {
        lang: {
            en: 'انگلیسی',
            fa: 'فارسی',
        },
        locale: 'زبان',
        design: 'طراحی',
        rtl_direction: 'چینش راست به چپ',
        theme: 'تم',
        themes: 'تم‌ها',
        light: 'روشن',
        dark: 'تیره',
        primary: 'اصلی',
        secondary: 'فرعی',
        accent: 'اکسنت',
        error: 'خطا',
        blog_title: 'عنوان وبلاگ',
        blog_title_hint:'عنوان زیبا برای وبلاگ خود انتخاب کنید'
    },

    pages: {
        post:{
            write: 'نوشتن پست جدید'
        },
        new: {
            post: 'پست جدید',
        },
        dashboard:"داشبورد",
        lang: {
            en: 'انگلیسی',
            fa: 'فارسی',
        },
        home: 'ادمین',
        auth: {
            login: 'ورود',
            register: {
                admin: "ثبت مدیر"
            }
        },
        about: 'راهنما',
        settings:'تنظیمات'
    },
    auth: {
        login: {
            title: 'پنل ادمین',
            email: 'ایمیل',
            password: 'رمز عبور',
            submit: 'ورود'
        },
        register: {
            title: 'ثبت نام مدیر',
            name: 'نام',
            email: 'ایمیل',
            password: 'رمز عبور',
            submit: 'ثبت'
        },
    },
    website: {
        title: "وبلاگ"
    },
    logout: 'خروج'
}