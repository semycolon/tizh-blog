module.exports = {
    visit_website: "Visit Website",
    editor: {
        placeholder: {
            text:'Type Something Great...'
        }
    },
    loading:'Loading',
    dashboard:{
        headline:'Your Posts',
        no_post:'Nothing yet',
        all_posts: 'All Posts',
        drafts: 'Drafts',
        last_change:'Last Changes',
        delete: 'Delete Post',
        edit:'Edit Post',
        copy_link:"Copy Short Link",
        published:'Published Posts',
        published_at:'Published at',
    },
    create_new_post:'Create New Post',
    hello: 'hello',
    submit:'Submit',
    reset:"Reset Default",
    _dashboard:'Dashboard',
    write_new_post:'Write New Post',
    _settings:'Settings',
    no_title:'No Title',
    settings: {
        lang: {
            en: 'English',
            fa: 'Persian',
        },
        locale: 'Locale',
        design: 'Design',
        rtl_direction: 'Right To Left Direction',
        theme: 'Theme',
        themes: 'Themes',
        light: 'Light',
        dark: 'Dark',
        primary: 'Primary',
        secondary: 'Secondary',
        accent: 'Accent',
        error: 'Error',
        blog_title:'Blog Title',
        blog_title_hint:'Choose a nice title for your blog'
    },
    pages: {
        post: {
            write: 'Write Post'
        },
        dashboard:'Dashboard',
        home: 'Dashboard',
        auth: {
            login: 'Login'
        },
        about: 'About',
        notfound: 'Not Found',
        settings: 'Settings',
        'null':'',
    },
    auth: {
        login: {
            title: 'Login',
            email: 'Email',
            password: 'Password',
            submit: 'Submit',
        },
        register: {
            title: 'Register Admin',
            name: 'Name',
            email: 'Email',
            password: 'Password',
            submit: 'Submit'
        },
    },
    website: {
        title: "Weblog"
    },
    title:'Title',

}