const PN = require("persian-number");
const m = require('moment');
const moment = m.default || m;

const units = {
    'years': {
        label: 'سال'
    },
    'months': {
        label: 'ماه'
    },
    'weeks': {
        label: 'هفته'
    },
    'days': {
        label: 'روز'
    },
    'hours': {
        label: 'ساعت'
    },
    'minutes': {
        label: 'دقیقه'
    },
    'seconds': {
        label: 'ثانیه'
    }
};
module.exports = function (arg1, arg2) {
    let m1 = moment(arg1),
        m2 = moment(arg2);

    for (let unit in units) {
        let diff = m1.diff(m2, unit);
        let difference = diff < 0 ? diff * -1 : diff;
        if (difference < 1) {
            continue;
        }
        let when = diff < 0 ? 'بعد' : 'قبل';
        return `${PN.convert(difference)} ${units[unit].label} ${when}`;
    }
    return 'لحظاتی قبل'
}