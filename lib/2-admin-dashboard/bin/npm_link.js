const modules = ['@tizh-blog/time-diff', '@tizh-blog/persian-time-diff'];

let shell = require('shelljs');

console.log('Linking Lib Packages for', require('../package.json').name);

const stdout = shell.exec('npm list -g --depth 0',{silent:true}).stdout;

for (let module of modules) {
    if (stdout.indexOf(module) > -1 ) {
        console.log('linking module', module, 'from global packages');
        shell.exec('npm link ' + module);
    }else{
        console.error('module not available globally,module:', module,"did you run 'npm install' on root project directory ?");
    }
}
