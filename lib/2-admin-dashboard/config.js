const config = require('@tizh/config');

async function getConfig() {
    let obj = await import('./../../src/app.config.js');
    config.setBulk(obj.default ? obj.default : obj);
    return config;
}

module.exports = getConfig;
