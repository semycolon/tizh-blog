import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store/vuex-store'
import setupVuetify from './plugins/vuetify';
import getConfig from '../config';
import axios from 'axios'
import VueAxios from 'vue-axios'

import setupI18n from "./plugins/i18n";
import setupAxios from './plugins/axios'


//translation
import VueI18n from 'vue-i18n'
import checkAuth from "./plugins/auth";
import fetchSettings from "./plugins/settings";
import setupMq from "./plugins/vue-mq";
import initMediumEditor from "./plugins/medium-editor";
import initDate from "./plugins/moment";
import GlobalMixins from "./mixins/GlobalMixins.js";

Vue.use(VueI18n)
Vue.mixin(GlobalMixins)

//todo get websites settings

async function bootstrap() {
    Vue.config.productionTip = false
    const config = await getConfig();
    Vue.prototype.$config = config;

    //step one setup axios
    let axios = setupAxios(config);

    //step two fetch settings
    let settings = await fetchSettings(config, axios);
    Vue.prototype.$isRtl = settings.design.rtl;
    Vue.prototype.$locale = settings.locale;
    Vue.prototype.$blog_title = settings.blog_title;

    //step three
    let i18n = await setupI18n(settings.locale);
    const vuetify = setupVuetify(settings.design);
    initMediumEditor()
    setupMq()
    initDate();


    await checkAuth(config, axios);

    let root = new Vue({
        i18n,
        router,
        store,
        vuetify,
        render: h => h(App)
    }).$mount('#app')


}

bootstrap();