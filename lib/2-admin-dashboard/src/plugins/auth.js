async function checkAuth(config,axios) {
    let url = config.urls.api.auth.base + '/' + config.urls.api.auth.admin.check
    try {
        let res = await axios.get(url);
        if (res.data) {
            if (res.data.no_admin) {
                window.no_admin = true;
            } else {
                try {
                    localStorage.setItem('user', JSON.stringify(res.data));
                } catch (e) {
                    console.log(e);
                }
                window.authenticated = true;
            }
        }else{
            throw new Error('empty body!');
        }
    } catch (err) {
        console.error('auth.js error',err);
    }
}

export default checkAuth