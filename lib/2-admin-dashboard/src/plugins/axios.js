import axios from "axios";
import Vue from "vue";
import VueAxios from "vue-axios";

function setupAxios(config) {
    axios.defaults.baseURL = process.env.VUE_APP_BASE_URL
    Vue.use(VueAxios, axios)
    Vue.config.productionTip = false

    axios.interceptors.request.use(
        config => {
            const token = localStorage.getItem("token");
            if (token) {
                config.headers.token = token;
            }
            return config;
        },
        error => Promise.reject(error)
    );
    return axios;
}


export default setupAxios