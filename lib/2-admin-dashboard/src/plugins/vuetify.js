import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify, {
    options: {
        customProperties: true
    }
});

const setupVuetify = function (options) {
    if (options.rtl) {
        import('../../public/fonts/sahel-font/font-face.css');
    }

    return new Vuetify({
        ...options,
        /*icons: {
            iconfont: 'mdiSvg',
        },*/
    });
}

export default setupVuetify
