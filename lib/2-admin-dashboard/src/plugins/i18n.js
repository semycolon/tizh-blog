import VueI18n from "vue-i18n";

const setupI18n = async function (locale) {
    if (!locale) {
        locale = 'en';
    }
    const it = await import('./../../../../lang/' + locale + '.js');
    const obj = it.default ? it.default : it
    let messages = {
        [locale]: {
                ...obj,
        }
    }

    return new VueI18n({
        locale: locale,
        messages,
    });
};
export default setupI18n;