import Vue from 'vue';
import moment from 'moment';
function initDate() {
    Vue.prototype.$m = moment;
}

export default initDate;