async function fetchSettings(config, axios) {
    let url = config.urls.api.settings.base + '/' + config.urls.api.settings.fetch;
    let res = await axios.get(url);
    if (res.data) {
        return res.data;
    }
    return {}
}

export default fetchSettings;