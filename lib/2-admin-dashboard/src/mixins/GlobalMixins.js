const PersianDate = require('persian-date');
const diffDateEn = require('@tizh-blog/time-diff');
const diffDateFa = require('@tizh-blog/persian-time-diff');
export default {
    methods: {
        async onCreateNewPost() {
            let url = this.$config.urls.api.posts.base + '/' + this.$config.urls.api.posts.create;
            let res = await this.$http.get(url,);
            if (res.data) {
                await this.$router.push({name:'post.write',params:{id: res.data.id}})
            }else{
                // todo show error
            }
        },
        diffDate(arg) {
            if (this.$locale == 'fa') {
                return diffDateFa(new Date(),arg);
            }else{
                return diffDateEn(new Date(),arg);
            }
        },
        formatDate(arg) {
            let format;
            let m = this.$m(arg);
            if (this.$locale == 'fa') {
                let date = new Date(arg);
                let pDate = new PersianDate(date);
                return pDate.format('YYYY/M/D');
            } else {
                format = 'YYYY/M/D'
                return m.format(format)
            }
        },
        formatDateTime(date) {
            let format;
            let m = this.$m(date);
            if (this.$locale == 'fa') {
                format = 'jYYYY/jM/jD  HH:mm';
            } else {
                format = 'YYYY/M/D  HH:mm'
            }
            return m.format(format)
        },
    }
}