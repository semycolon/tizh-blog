import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import axios from 'axios'

const SET_DRAWER = 'SET_DRAWER'

export default new Vuex.Store({


// state
    state: {
        navigation_drawer: null,
    },

// getters
    getters: {

    }
    ,
    mutations: {
        [SET_DRAWER](state, {value}) {
            state.navigation_drawer = value
        },
    },
    actions: {
        setDrawer ({ commit }, payload) {
            commit(SET_DRAWER, { value: payload })
        },
    }
})
