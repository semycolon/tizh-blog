import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginPage from "../pages/auth/login/login-page.vue";


import Home from "../pages/Home";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'dashboard',
        component: () => import(/* webpackChunkName: "about" */ '../pages/dashboard/dashboard-page.vue')
    },
    {
        path: '/auth/login',
        name: 'auth.login',
        component: () => import("../pages/auth/login/login-page.vue"),
    },
    {
        path: '/settings',
        name: 'settings',
        component: () => import("../pages/settings/settings-page.vue"),
    },{
        path: '/post-write/:id',
        name: 'post.write',
        component: () => import("../pages/posts/write-post-page.vue"),
    },
    {
        path: '/admin/register',
        name: 'admin.register',
        component: () => import("../pages/auth/register-admin/register-admin-page.vue"),
    },
    {
        path: '/about',
        name: 'about',
        component: () => import(/* webpackChunkName: "about" */ '../pages/About.vue')
    },
    {
        path: '*',
        name: 'notfound',
        component: () => import(/* webpackChunkName: "about" */ '../components/NotFound.vue'),
    }
]

const router = new VueRouter({
    mode: 'hash',
    //base: process.env.BASE_URL,
    routes
})


router.beforeEach((to, from, next) => {
    if (window.no_admin) {
        if (to.name == 'admin.register') {
            return next();
        } else {
            return next({name: 'admin.register'});
        }
    }
    if (to.name !== 'auth.login' && !window.authenticated) {
        return next({name: 'auth.login'});
    }
    if (window.authenticated) {
        if (to.name == 'auth.login' || to.name == 'admin.register') {
            return next({name: 'home'})
        }
    }
    next();
})

export default router
