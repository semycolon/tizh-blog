const rimraf = require("rimraf");
const generator = require('@nuxt/generator');
const builder = require('@nuxt/builder');
const webpack = require('@nuxt/webpack');
const core = require('@nuxt/core');
const {join,} = require('path');
const fs = require('fs');
const dirName = __dirname;
const dynamicMiddleware = require('express-dynamic-middleware');
const {generateSettingsRepository} = require("../1-blog-data/repositories/settings");

let cache = {};

async function getNuxt(options) {
    const {Nuxt} = core;
    const nuxt = new Nuxt(options);
    await nuxt.ready();
    return nuxt
}

async function getBuilder(nuxt) {
    const {Builder} = builder;
    const {BundleBuilder} = webpack;
    return new Builder(nuxt, BundleBuilder)
}

async function getGenerator(nuxt) {
    const {Generator} = generator;
    const builder = await getBuilder(nuxt);
    return new Generator(nuxt, builder)
}

module.exports.generateDynamicMiddleware = function (env, forceRebuild) {
    return function (req, res, next) {
        if (!cache[env.host_name]) {
            const dynamic = dynamicMiddleware.create();
            dynamic.use((req, res, next) => {
                res.sendFile(join(dirName, './static/building.html'));
            });
            generateNuxt(env, forceRebuild)
                .then(nuxt => {
                    dynamic.clean();
                    dynamic.use(nuxt.render)
                })
            cache[env.host_name] = dynamic;
        }
        return cache[env.host_name].handle()(req, res, next);
    };
};

module.exports.generateMiddlewareAsync = async function (env, forceRebuild) {
    let nuxt = await generateNuxt(env, forceRebuild);
    return nuxt.render;
}

async function generateNuxt(env, forceRebuild) {
    try {
        let {host_name, db_path} = env;
        let settings = generateSettingsRepository({host_name, db_path});

        let config = require(join(dirName, './nuxt.config.js'));
        config.srcDir = join(dirName, '/');
        config.rootDir = config.srcDir;
        config.buildDir = join(dirName, '.nuxt', host_name || '');
        config.publicPath = config.buildDir;
        config.dev = false;
        config.env = env;
        config.vuetify = settings.read().design;


        const nuxt = await getNuxt(config);
        await nuxt.ready();

        if (forceRebuild || !fs.existsSync(config.buildDir)) {
            const generator = await getGenerator(nuxt);
            const {errors} = await generator.generate({
                init: true,
                build: true,
            });
        }

        return nuxt
        console.log('generate Done');
    } catch (e) {
        console.error('error while builidng nux app', e);
    }
}

module.exports.setup = function ({host_app_instance, host_name, db_path,}) {
    let middleware = module.exports.generateDynamicMiddleware({host_name, db_path});
    host_app_instance.use(middleware);
};


module.exports.resetNuxtAppInstance = async function ({host_name, db_path}) {
    cache[host_name] = undefined;
    rimraf.sync(join(dirName, '.nuxt', host_name))
    const nuxt = await generateNuxt({host_name, db_path}, true)
    const dynamic = dynamicMiddleware.create();
    dynamic.clean();
    dynamic.use(nuxt.render);
    cache[host_name] = dynamic;
}
