const nuxtModule = require('./server/app.js');
const http = require('http');
const express = require('express');
const mainApp = express();

const {join} = require('path');

(async () => {
  const port = 8085;
  const db_path = join(__dirname,'../../playground');
  await nuxtModule.setup({
    host_app_instance: mainApp,
    host_name: 'demo2',
    db_path,
  })

  const server = http.createServer(mainApp);
  server.listen(port);
  console.log('server might be listening on http://localhost:' + port);
})()
