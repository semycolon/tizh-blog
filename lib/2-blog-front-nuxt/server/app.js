const express = require('express')
const consola = require('consola')
const {Nuxt, Builder} = require('nuxt')
const app = express()

// Import and Set Nuxt.js options
const config = require('../nuxt.config.js')
config.dev = process.env.NODE_ENV !== 'production'

module.exports.setup = async function ({host_app_instance, host_name, db_path,}) {
    config.env = {
        ...config.env,
        host_name,
        db_path,
    };

    // Init Nuxt.js
    const nuxt = new Nuxt(config)

    await nuxt.ready()

    // Build only in dev mode
    if (config.dev) {
        const builder = new Builder(nuxt)
        await builder.build()
    }


    // Give nuxt middleware to express
    host_app_instance.use(nuxt.render)
}
