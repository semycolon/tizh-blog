const fs = require('fs');
const {resolve, join} = require('path');
const dataModule = require('../../1-blog-data')
export default (context, inject) => {
    // Set the function directly on the context.app object
    context.db = dataModule.generateRepositories({host_name:context.env.host_name,db_path:context.env.db_path})
    context.app.myInjectedFunction = string => console.log('Okay, another function', string)
}
