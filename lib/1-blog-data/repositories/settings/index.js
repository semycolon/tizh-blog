const JsonLite = require('@semycolon/jsonlite');
const {join, resolve} = require('path');
let defaultSettings = require('./app-default-settings.js');
const nuxtAppModule = require("../../../2-blog-front-nuxt");

module.exports.generateSettingsRepository = function ({host_name, db_path,}) {
    const db = JsonLite(join(db_path, host_name,), true);
    if (!db.settings) {
        db.settings = defaultSettings;
    }

    return{
        read:function() {
            return db.settings;
        },
        reset: async function () {
            db.settings = defaultSettings;
            await nuxtAppModule.resetNuxtAppInstance({host_name, db_path})
        },
        update: async function (arg) {
            db.settings = arg;
            await nuxtAppModule.resetNuxtAppInstance({host_name, db_path})
        }
    }
}



