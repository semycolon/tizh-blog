let settings = {
    locale: 'fa',
    design: {
        rtl: true,
        theme: {
            themes: {
                light: {
                    primary: '#00c1ee',
                    secondary: '#3d6cb9',
                    accent: '#00fff0',
                    error: '#b71c1c',
                },
            },
        },
    }
}
settings.blog_title = settings.locale == 'fa' ? 'وبلاگ من' : 'My Blog';

module.exports = settings
