const JsonLite = require('@semycolon/jsonlite');
const {join, resolve} = require('path');
module.exports.generatePostRepository = function ({host_name, db_path,}) {
    const db = JsonLite(join(db_path, host_name, 'posts' || './'), true);

    if (!db.posts) {
        db.posts = [];
    }

    function find() {
        return db.posts;
    }

    function findById(id) {
        return db.posts.find(it => it.id === id);
    }

    function updateOne(post) {
        let posts = db.posts;
        for (let i in posts) {
            let it = posts[i];
            if (it.id === post.id) {
                for (let postKey in post) {
                    posts[i][postKey] = post[postKey];
                }
                posts[i].updated_at = new Date();
                break;
            }
        }
        db.posts = posts;
    }

    return {
        find, findById, updateOne,
        lastEmptyPostId: db.last_empty_post_id,
        insertOne(post) {
            let prev = db.posts;
            prev.push(post);
            db.posts = prev;
        },
        deleteOne(id) {
            let posts = db.posts;
            for (let i in posts) {
                let it = posts[i];
                if (it.id === id) {
                    posts.splice(i, 1);
                    break;
                }
            }
            db.posts = posts;
        },
        findPublics() {
            return db.posts
                .filter(it => it.published)
                .sort((a, b) => new Date(a.date).valueOf() - new Date(b.date).valueOf());
        },
        findDrafts() {
            return db.posts
                .filter(it => it.has_draft)
                .sort((a, b) => new Date(b.updated_ad || b.created_at).valueOf() - new Date(a.updated_ad || a.created_at).valueOf())
                .reverse();
        }
    };
}