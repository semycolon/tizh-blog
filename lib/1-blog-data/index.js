const {generateSettingsRepository} = require("./repositories/settings");
const {generatePostRepository} = require("./repositories/post");
module.exports.generateRepositories = function ({host_name, db_path,}) {
    return {
        Post: generatePostRepository({host_name, db_path}),
        Settings: generateSettingsRepository({host_name, db_path})
    }
}