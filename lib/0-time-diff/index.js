const numberToText = require('number-to-text')
require('number-to-text/converters/en-us');

const m = require('moment');
const moment = m.default || m;
const units = {
    'years': {
        label: 'Year',
        plural_label: 'Years'
    },
    'months': {
        label: 'Month',
        plural_label: 'Months'
    },
    'weeks': {
        label: 'Week',
        plural_label: 'Weeks'
    },
    'days': {
        label: 'Day',
        plural_label: 'Days'
    },
    'hours': {
        label: 'Hour',
        plural_label: 'Hours'
    },
    'minutes': {
        label: 'Minute',
        plural_label: 'Minutes'
    },
    'seconds': {
        label: 'Second',
        plural_label: 'Seconds'
    }
};
module.exports = function (arg1, arg2) {
    let m1 = moment(arg1),
        m2 = moment(arg2);

    for (let unit in units) {
        let diff = m1.diff(m2, unit);
        let difference = diff < 0 ? diff * -1 : diff;
        if (difference < 1) {
            continue;
        }
        let when = diff < 0 ?  'After' : 'Before';
        return `${numberToText.convertToText(difference)} ${difference > 1 ? units[unit].plural_label : units[unit].label} ${when}`;
    }
    return 'Sometime'
}