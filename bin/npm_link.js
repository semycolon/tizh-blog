let shell = require('shelljs');
const {join} = require('path');
const fs = require('fs');

const lib_path = join(__dirname , '../lib')
if (!shell.which('npm')) {
    shell.echo('npm command not available ...');
    shell.exit(1);
}

shell.ls(lib_path).forEach(function (file) {
    const module_path = join(lib_path, file);
    if (fs.existsSync(join(module_path, 'package.json'))) {
        console.log('Linking Local Module',file);
        if (shell.exec('npm link ' + module_path).code !== 0) {
            console.error('Failed to link local module', file);
            shell.exit(1);
        }
    }
})
