const express = require('express');
const {initAdminFrontServe} = require("./lib/inits");
const {initFrontFilesServe} = require("./lib/inits");
const {initApiRoutes} = require("./api/api");
const {initGlobalMiddleware} = require("./lib/inits");

module.exports.generateApp = async function (payload) {
    let config = require('@tizh/config');
    config.setBulk(require('./app.config.js'));
    const {host_name, domain_name, base_url, db_path} = payload;

    const app = express();

    initGlobalMiddleware(app);
    initApiRoutes(app, {host_name, domain_name, base_url, db_path});
    initAdminFrontServe(app, '/admin');
    await initFrontFilesServe(app, {host_name, domain_name, base_url, db_path,});

    return app;
};