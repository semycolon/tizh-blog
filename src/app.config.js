let urls = {
    api: {
        auth: {
            base: 'auth',
            admin: {
                register: "admin/register",
                login: "admin/login",
                check: "admin/check",
            }
        },
        settings: {
            base: 'settings',
            fetch: 'fetch',
            update: 'update',
            reset: 'reset',
        },
        posts: {
            update:'update',
            publish:'publish',
            one:'one',
            base: 'posts',
            fetch: 'fetch',
            all: 'all',
            create: 'create',
            delete: 'delete',
        }
    },
    api_base_url: 'api',
}

let config = {
    urls,

};
module.exports = config;