const {generateRandomString} = require("../../lib/helpers/utils")

const JsonLite = require('@semycolon/jsonlite');
const {join, resolve} = require('path');

const {generateRepositories} = require('../../../lib/1-blog-data');


module.exports.generatePostsActions = function ({host_name, domain_name, base_url, db_path}) {
    const {Post} = generateRepositories({host_name, db_path});

    function fetch_all_public_posts(req, res, next) {
        return res.json(Post.findPublics())
    }

    function action_fetch_all_posts(req, res, next) {
        let published = Post.findPublics();
        let drafts = Post.findDrafts()
        res.json({drafts, published})
    }

    function action_action_action_fetch_all_posts(req, res, next) {
        let id = req.body.id;
        let post = Post.findById(id);
        if (post) {
            return res.json(post);
        } else {
            return res.status(400).json({msg: 'notfound'})
        }
    }

    function action_update_post(req, res, next) {
        let {id, title, draft_content} = req.body;
        let post = Post.findById(id);
        if (post) {
            if (title) {
                post.title = title;
            }
            if (draft_content) {
                post.draft_content = draft_content;
            }
            Post.updateOne(post);
            return res.json(post);
        } else {
            return res.status(400).json({msg: 'notfound'})
        }
    }

    function create_post(req, res, next) {
        if (Post.lastEmptyPostId) {
            let post = Post.findById(Post.lastEmptyPostId);
            if (post && !post.title && !post.draft_content) {
                return res.json({id: post.id});
            }
        }
        let id = generateRandomString(16);
        Post.insertOne({id, has_draft: true, created_at: new Date()});
        Post.lastEmptyPostId = id;
        res.json({id})
    }

    function action_delete_post(req, res, next) {
        let {id} = req.body;
        let post = Post.findById(id);
        if (post) {
            Post.deleteOne(id);
            res.json({msg: 'done'})
        } else {
            return res.status(404).json({msg: 'notfound'})
        }

    }

    function action_publish_post(req, res, next) {
        let {id} = req.body;
        let post = Post.findById(id);
        if (post) {
            post.content = post.draft_content;
            post.has_draft = false;
            post.published_at = new Date();
            post.published = true;
            Post.updateOne(post);
            res.json(post);
        } else {
            return res.status(404).json({msg: 'notfound'})
        }
    }

    const {check, validationResult} = require('express-validator');
    const create_posts_validators = [
        check('title').exists().withMessage('title' + ' isRequired'),
        check('slug').exists().withMessage('slug' + ' isRequired'),
        check('content').exists().withMessage('content' + ' isRequired'),
        //  check('tags').exists().withMessage('tags' + ' isRequired'),
        check('published').exists().withMessage('published' + ' isRequired'),
    ]

    const delete_post_validators = [
        check('slug').exists()
    ]

    return {
        fetch_all_public_posts,
        action_delete_post,
        create_post,
        action_fetch_all_posts,
        create_posts_validators,
        delete_post_validators,
        action_action_action_fetch_all_posts,
        action_update_post,
        action_publish_post,
    };
};
