const {generatePostsActions} = require("./PostsActions");
const config = require('@tizh/config');
const bodyParser = require('body-parser');
const express = require('express');
const {validationErrorHandler} = require("../auth/actions");

const {adminAuthGuard} = require("../auth/actions");

module.exports.generatePostsRouter = function ({host_name, domain_name, base_url, db_path}) {
    const app = express.Router()
    const actions = generatePostsActions({host_name, domain_name, base_url, db_path});
    const {
        fetch_all_public_posts, action_delete_post, create_post, delete_post_validators,
        action_fetch_all_posts, action_action_action_fetch_all_posts, action_update_post,action_publish_post
    } = actions;

    const {adminAuthGuard} = require('../auth/actions').generate({host_name, domain_name, base_url, db_path});

    app.use(bodyParser.json());

    let posts = config.urls.api.posts;
    app.get('/' + posts.fetch, fetch_all_public_posts)
    app.get('/' + posts.all, adminAuthGuard, action_fetch_all_posts)
    app.get('/' + posts.create, adminAuthGuard, create_post)
    app.post('/' + posts.delete, adminAuthGuard, action_delete_post)
    app.post('/' + posts.one, adminAuthGuard, action_action_action_fetch_all_posts)
    app.post('/' + posts.update, adminAuthGuard, action_update_post)
    app.post('/' + posts.publish, adminAuthGuard, action_publish_post)


    return app;
};