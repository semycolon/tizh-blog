const {validationResult} = require("express-validator");


const config = require('@tizh/config');
const cors = require('cors');
const {generatePostsRouter} = require("./posts");
const {generateSettingsRouter} = require("./settings");
const {generateAuthRouter} = require("./auth");


module.exports.initApiRoutes = function (app, {host_name, domain_name, base_url, db_path}) {
    let authRouter = generateAuthRouter({host_name, domain_name, base_url, db_path});
    let settingsRouter = generateSettingsRouter({host_name, domain_name, base_url, db_path});
    let postsRouter = generatePostsRouter({host_name, domain_name, base_url, db_path});
    let apiBaseUrl = config.urls.api_base_url;
    let authBaseUrl = config.urls.api.auth.base;
    let settingsBaseUrl = config.urls.api.settings.base;
    let postsBaseUrl = config.urls.api.posts.base;

    app.use(`/${apiBaseUrl}`, cors());
    app.use(`/${apiBaseUrl}/${authBaseUrl}`, authRouter);
    app.use(`/${apiBaseUrl}/${settingsBaseUrl}`, settingsRouter)
    app.use(`/${apiBaseUrl}/${postsBaseUrl}`, postsRouter);
};

