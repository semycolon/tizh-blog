const express = require('express');
const actionsModule = require('./actions')
const config = require('@tizh/config');
const bodyParser = require('body-parser');

module.exports.generateAuthRouter = function ({host_name, domain_name, base_url, db_path}) {
    const app = express.Router()
    const actions = actionsModule.generate({host_name, domain_name, base_url, db_path});
    const {
        registerAdminValidators, registerAdminAction, checkAdminAction,
        checkCreatedAdmin, loginAdminAction,
        loginAdminValidators, validationErrorHandler,adminAuthGuard
    } = actions;

    app.use(bodyParser.json());

    app.post('/' + config.urls.api.auth.admin.register, registerAdminValidators, validationErrorHandler, registerAdminAction)
    app.post('/admin/login', loginAdminValidators, validationErrorHandler, loginAdminAction)
    app.get('/' + config.urls.api.auth.admin.check, checkCreatedAdmin, adminAuthGuard, checkAdminAction)


    return app;
};