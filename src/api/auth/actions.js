const config = require('@tizh/config');
const {join} = require('path');
const JsonLite = require('@semycolon/jsonlite');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const {check, validationResult} = require('express-validator');
let cache = {};

function generateActions({host_name, domain_name, base_url, db_path}) {
    console.log('authGenerateActions called with', host_name, db_path);
    let obj = {};
    const db = JsonLite(join(db_path, host_name), true);
    const db_users = JsonLite(join(db_path, host_name, '/users'), true);

    let accessTokenSecret;
    if (db.jwt_secret) {
        accessTokenSecret = db.jwt_secret;
    } else {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync('secure_token', salt);
        db.jwt_secret = hash;
        accessTokenSecret = hash;
    }

    obj.checkAdminAction = function (req, res, next) {
        const {email, name} = db_users.admin;
        return res.json({email, name});
    };
    obj.checkCreatedAdmin = function (req, res, next) {
        if (!db_users.admin) {
            return res.json({no_admin: true})
        }
        next();
    };

    obj.validationErrorHandler = function (req, res, next) {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({errors: errors.array()});
        }
        return next();
    };
    obj.registerAdminAction = function (req, res, next) {
        if (!db_users.admin) {
            const {email, password, name} = req.body;
            const salt = bcrypt.genSaltSync(10);
            const hash = bcrypt.hashSync(password, salt);

            let admin = {
                role: 'admin',
                email,
                password: hash,
                name,
            };

            db_users.admin = admin;
            let token = generateJWT(admin);
            return res.json({msg: 'success', token})
        } else {
            return res.status(400).send();
        }

    };
    obj.registerAdminValidators = [
        check('email').exists().isEmail(),
        check('password').exists().isLength({min: 6, max: 256}),
        check('name').exists().isLength({min: 3, max: 56})
    ]

    function generateJWT(admin) {
        const accessToken = jwt.sign({email: admin.email, role: admin.role}, accessTokenSecret);
        return accessToken;
    }

    obj.loginAdminAction = function (req, res, next) {
        const {email, password} = req.body;
        let admin = db_users.admin;
        if (email == admin.email) {
            const result = bcrypt.compareSync(password, admin.password);
            if (result) {
                const accessToken = generateJWT(admin);
                return res.json({msg: 'success', token: accessToken, user: {email, name: admin.name}})
            }
        }
        return res.status(400).json({msg: 'errors.credentials.not_match'})
    };
    obj.loginAdminValidators = [
        check('email').exists().isEmail(),
        check('password').exists().isLength({min: 6, max: 256}),
    ]

    obj.adminAuthGuard = function (req, res, next) {
        let token = req.headers.token;
        if (token) {
            try {
                const decoded = jwt.verify(token, accessTokenSecret);
                req.user = decoded;
                return next();
            } catch (e) {
                console.error(e);
                return res.status(401).json({msg: 'errors.unauthorized'})
            }
        }
        return res.status(401).json({msg: 'errors.unauthorized'})
    };

    return obj;

};

module.exports.validationErrorHandler = function (req, res, next) {
    // Finds the validation errors in this request and wraps them in an object with handy functions
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({errors: errors.array()});
    }
    return next();
};

module.exports.generate = function ({host_name, domain_name, base_url, db_path}) {
    if (!cache[host_name]) {
        cache[host_name] = generateActions({host_name, domain_name, base_url, db_path});
    }
    return cache[host_name];
}