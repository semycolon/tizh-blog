const express = require('express');
const {generateSettingsActions} = require("./settings-actions");
const config = require('@tizh/config');
const bodyParser = require('body-parser');

module.exports.generateSettingsRouter = function ({host_name, domain_name, base_url, db_path}) {
    const app = express.Router();
    const actions = generateSettingsActions({host_name, domain_name, base_url, db_path});
    const {fetchSettings, resetSettings, updateSettings,} = actions;

    app.use(bodyParser.json());

    app.get('/' + config.urls.api.settings.fetch, fetchSettings)
    app.post('/' + config.urls.api.settings.reset, resetSettings)
    app.post('/' + config.urls.api.settings.update, updateSettings)

    return app;
};