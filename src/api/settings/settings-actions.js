const moduleData = require('../../../lib/1-blog-data')
module.exports.generateSettingsActions = function ({host_name, domain_name, base_url, db_path}) {
    const {Settings} = moduleData.generateRepositories({host_name, db_path})
    return {
        fetchSettings: function (req, res, next) {
            return res.json(Settings.read());
        },

        resetSettings: async function (req, res, next) {
            await Settings.reset()
            return res.json({msg: 'done'});
        },

        updateSettings: async function (req, res, next) {
            //todo validate settings
            const settings = req.body;
            await Settings.update(settings)
            return res.json({msg: 'done'})
        },
    }
};
