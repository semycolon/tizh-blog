module.exports = {
    blog_title: {
        type: 'string',
        label: 'blog_title',
        hint:'blog_title_hint',
    },
    locale: {
        type: 'enum',
        options: [
            {
                label: 'lang.fa',
                value: 'fa',
            }, {
                label: 'lang.en',
                value: 'en',
            },
        ],
    },
    design: {
        type: 'object',
        rtl: {
            label: 'rtl_direction',
            type: 'boolean'
        },
        theme: {
            type: 'object',
            themes: {
                type: 'object',
                light: {
                    type: 'object',
                    primary: {
                        type: 'color',
                    },
                    secondary: {
                        type: 'color',
                    },
                    accent: {
                        type: 'color',
                    },
                    error: {
                        type: 'color',
                    },
                },
            },
        },
    }
}