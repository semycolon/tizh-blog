const {resolve} = require('path');
const {serveVueDistFolder} = require("./helpers/init-helpers");
const frontNuxtModule = require('../../lib/2-blog-front-nuxt')
module.exports.initGlobalMiddleware = function (app) {

};

module.exports.initAdminFrontServe = function (app, base_url) {
    const dist_path = require('./../../lib/2-admin-dashboard/export.js').dist_path;
    serveVueDistFolder(dist_path, app, base_url);

};
module.exports.initFrontFilesServe = async function (app, {host_name, domain_name, base_url, db_path,}) {
    await frontNuxtModule.setup({host_app_instance: app, host_name, db_path,})
};
