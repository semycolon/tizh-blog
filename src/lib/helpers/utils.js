module.exports.generateRandomString = function (length) {
    //return Math.random().toString(36).substring(2, 2 + length) + Math.random().toString(36).substring(2, 2 + length);
    let a = '';
    for (; a.length < length;) a += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz"[(Math.random() * 60) | 0]
    return a;
};