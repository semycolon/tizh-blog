const {resolve, join} = require('path');
const fs = require('fs');
const express = require('express');

module.exports.serveVueDistFolder = function (dist_path, app, base_url) {
    const html_path = resolve(join(dist_path, 'index.html'))

    //check if dist files exists
    if (!(fs.existsSync(dist_path) && fs.existsSync(html_path))) {
        console.error('front-end :: dist folder does not exists, please make sure you build the project', dist_path, html_path);
        return;
    }

    //serve public files
    let filterJsCss = (req, res, next) => {
        let it = req.url;
        if (it.startsWith('/css') || it.startsWith('/js') || it.startsWith('/fonts')) {
            return express.static(dist_path)(req, res, next);
        } else {
            next();
        }
    };
    app.use(filterJsCss,);


    //serve index.html
    app.get(base_url, (req, res, next) => {
        let url = req.url;
        if (url.endsWith('/')) {
            url = url.substring(0, url.length - 1);
        }
        if (base_url.indexOf(url) > -1) {
            return res.sendFile(html_path);
        }
        next();
    });
};