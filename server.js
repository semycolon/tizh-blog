const blogAppModule = require('./index.js');
const http = require('http');
const express = require('express');
const mainApp = express();

const {join} = require('path');

async function main() {
    const port = 31221;
    const delayMiddleware = (req, res, next) => {
        setTimeout(() => {
            next();
        },1000)
    }
    // mainApp.use(delayMiddleware);

    const db_path = join(__dirname , 'playground');
    await blogAppModule.setup({
        host_app_instance: mainApp,
        host_name: 'demo3',
        db_path,
    })

    const server = http.createServer(mainApp);
    server.listen(port);
    console.log('server might be listening on http://localhost:' + port);

}

main();
