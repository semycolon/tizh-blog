const {join} = require('path');
const {generateApp} = require("./src/app");
module.exports.setup = async function ({host_app_instance, host_name, domain_name, base_url}) {
    const db_path = join(__dirname,'../tizh-blog-database')
    const app = await generateApp({host_name, domain_name, base_url, db_path})
    host_app_instance.use(app);
};

module.exports.settings = {};

module.exports.details = {
    name: 'Tizh blog',
    version: '1.0.0',
    description: 'a minimal and simple blog with admin page',
}
